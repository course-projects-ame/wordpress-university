<?php

function university_page_banner($args = NULL)
{
    if (!isset($args['title'])) {
        $args['title'] = get_the_title();
    }

    if (!isset($args['subtitle'])) {
        $args['subtitle'] = get_field('page_banner_subtitle');
    }

    if (!isset($args['photo'])) {
        if (get_field('page_banner_image_background') and !is_archive() and !is_home()) {
            $args['photo'] = get_field('page_banner_image_background')['sizes']['page-banner'];
        } else {
            $args['photo'] = get_theme_file_uri('/images/ocean.jpg');
        }
    }
?>
    <div class="page-banner">
        <div class="page-banner__bg-image" style="background-image: url(<?php echo $args['photo']; ?>);"></div>
        <div class="page-banner__content container container--narrow">
            <h1 class="page-banner__title"><?= $args['title']; ?></h1>
            <div class="page-banner__intro">
                <p><?= $args['subtitle']; ?></p>
            </div>
        </div>
    </div>
<?php
}

function university_load_resources()
{
    wp_enqueue_script('university-google-maps', '//maps.googleapis.com/maps/api/js?key=AIzaSyAvAWTQuF5dXEjybU2U55-dC0-NpzefryU', NULL, '1.0', true);
    wp_enqueue_script('university-js', get_theme_file_uri('/build/index.js'), array('jquery'), '1.0', true);
    wp_enqueue_style('university-fonts', 'https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i');
    wp_enqueue_style('university-font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style('university-main-css-1', get_theme_file_uri('/build/index.css'));
    wp_enqueue_style('university-main-css-2', get_theme_file_uri('/build/style-index.css'));
}

add_action('wp_enqueue_scripts', 'university_load_resources');

function university_features()
{
    register_nav_menu('university-menu', 'Header Menu');
    register_nav_menu('university-menu-footer-1', 'Footer Menu 1');
    register_nav_menu('university-menu-footer-2', 'Footer Menu 2');
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    add_image_size('professor-landscape', 400, 260, true);
    add_image_size('professor-portrait', 480, 650, true);
    add_image_size('page-banner', 1500, 350, true);
}

add_action('after_setup_theme', 'university_features');

function university_adjust_queries($query)
{
    if (!is_admin() && is_post_type_archive('campus') && $query->is_main_query()) {
        $query->set('post_per_page', -1);
    }

    if (!is_admin() && is_post_type_archive('program') && $query->is_main_query()) {
        $query->set('orderby', 'title');
        $query->set('order', 'ASC');
        $query->set('post_per_page', -1);
    }

    if (!is_admin() && is_post_type_archive('event') && $query->is_main_query()) {
        $today = date('Ymd');
        $query->set('meta_key', 'event_date');
        $query->set('orderby', 'meta_value_num');
        $query->set('order', 'ASC');
        $query->set('meta_query', array(
            array(
                'key' => 'event_date',
                'compare' => '>=',
                'value' => $today,
                'type' => 'numeric'
            )
        ));
    }
}

add_action('pre_get_posts', 'university_adjust_queries');
